import 'dart:io';

class Calculator {
  var operator;
  double first = 0;
  double second = 0;
  double sum = 0;

  Calculator(double first, double second) {
    this.first = first;
    this.second = second;
    operator = operator;
    sum = 0;
  }

  void checkOperator(var operator) {
    switch (operator) {
      case "+":
        sum = sum + add(first, second);
        break;
      case "-":
        sum = sum + subtract(first, second);
        break;
      case "*":
        sum = sum + multiply(first, second);
        break;
      case "/":
        sum = sum + divide(first, second);
        break;
      default:
        return;
    }
  }

  double add(double first, double second) {
    return first + second;
  }

  double subtract(double first, double second) {
    return first - second;
  }

  double multiply(double first, double second) {
    return first * second;
  }

  double divide(double first, double second) {
    return first / second;
  }

  double getSum() {
    return sum;
  }
}

void main() {
  var calculate = false;

  print("Enter the number :");
  double first = double.parse(stdin.readLineSync()!);
  while (calculate == false) {
    print("Enter the operator :");
    String? operator = stdin.readLineSync();
    if (operator == 'c' || operator == 'C') {
      break;
    }
    print("Enter the number :");
    double second = double.parse(stdin.readLineSync()!);
    Calculator cal = new Calculator(first, second);
    cal.checkOperator(operator);
    print(cal.getSum());
  }
}
